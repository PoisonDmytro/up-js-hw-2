// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Якщо роздивлятису у рамках JS, то наприклад робота з JSON, DOM , налаштування винятків чи відлову помилок для різноманітних функцій, запобігання крашу програми при випадковій помилці і т.д.

//-----------------------------
const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
//----------------------------------
const root = document.querySelector("#root");
const list = document.createElement("ul");
list.classList.add("list")
root.append(list);

for (let i = 0; i < books.length; i++) {
    try {
        if (!books[i].author) {
            throw new Error("Відсутній автор книги: " + JSON.stringify(books[i]));
        }
        if (!books[i].name) {
            throw new Error("Відсутня назва книги: " + JSON.stringify(books[i]));
        }
        if (!books[i].price) {
            throw new Error("Відсутня ціна книги: " + JSON.stringify(books[i]));
        }

        let li = document.createElement("li");
        li.classList.add("list_element");

        let author = document.createElement("h2");
        author.textContent = "Автор: " + books[i].author;

        let name = document.createElement("p");
        name.textContent = "Назва: " + books[i].name;

        let price = document.createElement("p");
        price.textContent = "Ціна: " + books[i].price + " ₴";

        li.appendChild(author);
        li.appendChild(name);
        li.appendChild(price);

        list.appendChild(li);
    } catch (error) {
        console.error("Помилка під час обробки об'єкта книги:", error.message);
    }
}
